package com.yopan.javaservlet;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.*;

@WebServlet(name = "YopanServlet", value = "/yopan-servlet")
public class YopanServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource can not be accessed yet.");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        out.println("<html><head><title>User Successfully Registered</title></head><body>");

        try {
            String email = request.getParameter("email");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String password2 = request.getParameter("password2");

            boolean isDataValid = isDataValid(username, email);
            boolean isPasswordValid = isPasswordValid(password, password2);

            if (isDataValid && isPasswordValid) {
                out.println("<h1>User Successfully Registered</h1>");
                out.println("<p>Verify your details.</p>");
                out.println("<p>Email: " + email + "</p>");
                out.println("<p>Username: " + username + "</p>");
                out.println("Did confirmation password match? " + (isPasswordValid ? "Yes" : "No"));
                out.println("<p><strong>Important: </strong>keep your login details safe</p>");
            } else {
                throw new java.lang.RuntimeException("There was an error with your data. Please try again.");
            }
        } catch (Exception exception) {
            out.println("<h1>There was an error</h1>");
            out.println("<p>Please try again: <a href=\"register.html\">register</a></p>");
            System.err.println(exception);
        } finally {
            out.println("</body></html>");
        }
    }

    public static boolean isPasswordValid(String password, String password2) {
        return password.equals(password2) && password2.length() > 1;
    }

    public static boolean isDataValid(String username, String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        String usernameRegex = "^[A-Za-z]\\w{5,29}$";

        boolean isUsernameValid = isStringValid(username, usernameRegex);
        boolean isEmailValid = isStringValid(email, emailRegex);

        return isUsernameValid && isEmailValid;
    }

    public static boolean isStringValid(String string, String regex) {
        Pattern pattern = Pattern.compile(regex);

        if (string == null) {
            return false;
        }

        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
}
